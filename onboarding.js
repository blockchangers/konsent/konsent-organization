require('dotenv').config()
const csv                     = require('csvtojson');
const {map, compose, curry}   = require('ramda');
const uuidv4                  = require('uuid/v4');
const EthrDID                 = require('ethr-did');
const request                 = require('request');
const util                    = require('ethereumjs-util');
const levelup                 = require('levelup');
const leveldown               = require('leveldown');
const {sgMail, emailAsFormattedString}                  = require('./email');
const wallet                  = require('./wallet');
const web3                    = require('./web3');
const {logger, messageLogger} = require('./logger');
const repository              = require('./db/repository');

const ORGANIZATION_NUMBER           = process.env.ORGANIZATION_NUMBER;
const ORGANIZATION_NAME             = process.env.ORGANIZATION_NAME;
const ORGANIZATION_CALLBACK_URL     = process.env.ORGANIZATION_CALLBACK_URL;
const ORGANIZATION_DOMAIN_NAME      = process.env.ORGANIZATION_DOMAIN_NAME;
const MOCK_USERS                    = process.env.MOCK_USERS_FILE;
const KONSENT_REGISTRY_ORG_URL      = process.env.KONSENT_REGISTRY_ORG_URL;
const KONSENT_MOBIL_DEPLOYMENT_PATH = process.env.KONSENT_MOBIL_DEPLOYMENT_PATH;
const PATH_TO_EXPORTED_WALLET       = process.env.PATH_TO_EXPORTED_WALLET;
const WALLET_PASSWORD               = process.env.WALLET_PASSWORD;
const KONSENT_ONBOARDING_URL        = process.env.KONSENT_ONBOARDING_URL;
const KONSENT_REGISTRY_BEARER_TOKEN = process.env.KONSENT_REGISTRY_BEARER_TOKEN;

const DB_PATH = process.env.DB_PATH || './users';
const levelDbStore = levelup(leveldown(DB_PATH));

const addUuid = users => map(user => ({ ...user, uuid: uuidv4()}), users);
const addIssuerDid = curry((did, users) => map(user => ({ ...user, issuer: did}), users));

const store = curry((store, users) => {
  map(user => {
    return repository.setUser(store, user.uuid, {
      phone: user.phone,
      email: user.email,
      uuid: user.uuid,
      creation: Date.now(),
      issuer: user.issuer
    });
  }, users);
  logger.info('storing users from csv to leveldb storage');
  return Promise.resolve(users);
});

const composeEmail = user => {
  const msg = {
    to:       user.email,
    from:     'installation@konsent.com',
    subject:  `Your personalized Konsent App is now ready`,
    html: emailAsFormattedString(user),
  };
  return msg;
}

const send = curry((emailClient, users)=> {
  return new Promise((resolve, reject) => {
    const emails    = map(composeEmail, users)
    const requests  = map((email) => emailClient.send(email), emails)
    Promise.all(requests).then((data) => {
      logger.info('sending emails to users');
      resolve(users);
    }).catch((err) => {
      reject(new Error('failure: sending emails ', err));
    });
  });
});

const createRegistryEntry = curry((entry) => {
  logger.info('creating an organization registry entry');
  return new Promise((resolve, reject) => {
    request({
      auth: {
        bearer: KONSENT_REGISTRY_BEARER_TOKEN
      },
      url: KONSENT_REGISTRY_ORG_URL,
      method: 'POST',
      json: {
        ...entry
      }
    }, (error, response, body) => {
      if(error) {
        logger.error(error);
        reject(error);
      } else {
        resolve(body);
      }
    });
  });
});

const privateKeyToPublic = compose(util.bufferToHex, util.privateToPublic);

(async () => {
    const loadedWallet = await wallet.load(PATH_TO_EXPORTED_WALLET, WALLET_PASSWORD);

    const issuer = new EthrDID({
      address       : loadedWallet.address,
      privateKey    : loadedWallet.privateKey.substring(2),
      provider      : web3.currentProvider
    });

    const publicKey = privateKeyToPublic(loadedWallet.privateKey);
    const users     = await csv().fromFile(MOCK_USERS);
    const persist   = compose(store(levelDbStore), addUuid, addIssuerDid(issuer.did))

    const entry = {
      organizationNumber: ORGANIZATION_NUMBER,
      name: ORGANIZATION_NAME,
      callbackUrl: ORGANIZATION_CALLBACK_URL,
      did: issuer.did,
      domain: `${ORGANIZATION_DOMAIN_NAME}`,
      address: issuer.address,
      publicKey: publicKey
    };

    persist(users)
    .then(send(sgMail))
    .then(createRegistryEntry(entry))
    .then(() => process.exit(0))
    .catch(err => {
      logger.error(err);
      process.exit(1);
    });
  }
)();
