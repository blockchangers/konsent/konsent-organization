const pify        = require('pify');
const fs          = pify(require('fs'));
const ethers      = require('ethers');

const privateKeyFromSeed = (seed) => {
  const {privateKey} = ethers.Wallet.fromMnemonic(seed);
  return privateKey;
};

const generateRandomSeed = () => {
 const {mnemonic} = ethers.Wallet.createRandom();
 return mnemonic;
};

const createEncrypted = (seed, password) => {
  const wallet = ethers.Wallet.fromMnemonic(seed);
  return wallet.encrypt(password, (progress) => {
    console.log("Encrypting: " + parseInt(progress * 100) + "%");
  });
};

const create = (seed) => {
  return ethers.Wallet.fromMnemonic(seed);
};

const load = async(path, password) => {
  try {
    const importedWallet = await fs.readFileSync(path, {encoding: 'utf-8'});
    const wallet = await ethers.Wallet.fromEncryptedJson(JSON.parse(importedWallet), password);
    return Promise.resolve(wallet);
  } catch (error) {
    return Promise.reject(error);
  }
};

module.exports = {
  createEncrypted,
  create,
  load,
  privateKeyFromSeed,
  generateRandomSeed
};

