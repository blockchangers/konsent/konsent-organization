const R = require('ramda');
const d3 = require('d3');

const extract = jwt => R.map(claim => claim, jwt.payload.claims);

const concat = R.unapply(R.unnest)

const countBy = R.countBy;

const groupByType = claims => d3.nest().key((c) => c.type).object(claims);

const totalNumberOfOccurences = R.compose(R.sum, R.values);

const withPercentage = R.curry((total, list) => R.mapObjIndexed((num, key, obj) => ({ percentage: num / total, count: num }),list));

const rank = list => {
  let rank = 1;
  for (let i = 0; i < list.length; i++) {
    if (i > 0 && list[i].value.count < list[i - 1].value.count) {
      rank++;
    }
    list[i].value.rank = rank;
  }
  return list;
};

module.exports = {
  rank,
  totalNumberOfOccurences,
  withPercentage,
  countBy,
  extract,
  concat,
  groupByType
};
