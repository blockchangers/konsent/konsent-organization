#!/usr/bin/env node

require('dotenv').config();

const {Async, constant}                       = require('crocks');
const wallet                                  = require('./wallet');
const {writeFileAsync, createFormattedJSON}   = require('./helpers');
const {messageLogger}                         = require('./logger');

const WALLET_PASSWORD         = process.env.WALLET_PASSWORD;
const PATH_TO_SEED_FILE       = process.env.PATH_TO_SEED_FILE;
const PATH_TO_EXPORTED_WALLET = process.env.PATH_TO_EXPORTED_WALLET;

(async () => {
  const generateRandomSeed  = Async.of(wallet.generateRandomSeed());
  const createWallet        = Async.fromPromise(wallet.createEncrypted);

  generateRandomSeed
  .chain(seed =>
    writeFileAsync(PATH_TO_SEED_FILE, seed)
    .map(constant(seed))
    .chain(seed =>
      createWallet(seed, WALLET_PASSWORD)
      .chain(wallet =>
        writeFileAsync(PATH_TO_EXPORTED_WALLET, createFormattedJSON(wallet))
        .map(constant({wallet, seed}))
      )
    )
  )
  .fork(
    (error) => {
      console.error('error: failed creating wallet and error', error);
      process.exit(1);
    },
    ({wallet, seed}) => {
      messageLogger("Wallet", wallet);
      messageLogger("Seed", seed);
      process.exit(0);
    }
  );
})();
