
require('dotenv').config();

const FRONTEND_DOMAIN           = process.env.FRONTEND_DOMAIN;

const allowedOrigins = ['http://localhost:8080', 'https://obos-eierskifte.firebaseapp.com'];

if(FRONTEND_DOMAIN) {
  allowedOrigins.push(FRONTEND_DOMAIN);
}

const corsSecure = () => {
  return cors({
  origin: function(origin, callback){
    // allow requests with no origin (like mobile apps or curl requests)
    if(!origin) return callback(null, true);

    if(allowedOrigins.indexOf(origin) === -1){
      const msg = 'The CORS policy for this site does not allow access from the specified Origin.';
      return callback(new Error(msg), false);
    }
    return callback(null, true);
  }
})};

module.exports = {
  corsSecure
};
