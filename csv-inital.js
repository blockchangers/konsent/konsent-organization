#!/usr/bin/env node

require('dotenv').config();

const {map, compose, curry}   = require('ramda');
const csv                     = require('csvtojson');
const levelup                 = require('levelup');
const leveldown               = require('leveldown');
const uuidv4                  = require('uuid/v4');
const {messageLogger}         = require('./logger');
const repository              = require('./db/repository');

const CSV_FILE = process.env.CSV_FILE;

const DB_PATH = process.env.DB_PATH || './users';
const levelDbStore = levelup(leveldown(DB_PATH));

const addUuid = users => map(user => ({ ...user, uuid: uuidv4()}), users);

const store = curry((store, users) => {
  map(user => {
    return repository.setUser(store, user.uuid, {
      phone: user.phone,
      email: user.email,
      crmId: user.id,
      uuid: user.uuid,
      creation: Date.now(),
      dob: user.dob,
      address: user.address,
      notified: false
    });
  }, users);

  messageLogger('storing user record with uuid as key', users);
  return Promise.resolve(users);
});

(async () => {
  try {
    const users     = await csv().fromFile(CSV_FILE);
    const persist   = compose(store(levelDbStore), addUuid);

    await persist(users);
    process.exit(0);
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
}
)();
