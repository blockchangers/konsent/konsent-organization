const http                                = require('http');
const express                             = require('express');
const cors                                = require('cors');
const morgan                              = require('morgan');
const bodyParser                          = require('body-parser');
const EthrDID                             = require('ethr-did');
const R                                   = require('ramda');
const helmet                              = require('helmet');
const levelup                             = require('levelup');
const leveldown                           = require('leveldown');
const {verifyJWT, decodeJWT}              = require('did-jwt');
const wallet                              = require('./wallet');
const web3                                = require('./web3');
const {logger, messageLogger}             = require('./logger');
const repository                          = require('./db/repository');

require('ethr-did-resolver')();

const {UserNotFound, JwtValidation, DatabaseError, InvalidArgument} = require('./shared/errors');

const PATH_TO_EXPORTED_WALLET       = process.env.PATH_TO_EXPORTED_WALLET;
const WALLET_PASSWORD               = process.env.WALLET_PASSWORD;
const GATEWAY_PORT                  = process.env.GATEWAY_PORT;

const DB_PATH = process.env.DB_PATH || './users';
const store = levelup(leveldown(DB_PATH));

messageLogger("Environment", process.env);

let signer = null;

(async () => {
  try {
    const loadedWallet = await wallet.load(PATH_TO_EXPORTED_WALLET, WALLET_PASSWORD);
    signer = new EthrDID({
      address: loadedWallet.address,
      privateKey: loadedWallet.privateKey.substring(2),
      provider: web3.currentProvider
    });
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
})()

const config = {
	"port"             : GATEWAY_PORT,
	"bodyLimit"        : "100kb"
};

const app = express();

app.server = http.createServer(app);

app.use(morgan('dev'));

app.use(cors());
app.use(helmet());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: config.bodyLimit }));

const verify = async(jwt) => {
  return new Promise(async(resolve, reject) => {
    try {
      await verifyJWT(jwt);
      messageLogger('verified jwt', jwt);
      resolve(decodeJWT(jwt));
    } catch (error) {
      reject(new JwtValidation(error));
    }
  });
};

const createNewUserRecord = (store, { did, phone, email }) => {
  return new Promise(async(resolve, reject) => {
    try {
      const user = repository.setUser(store, did, {
        phone,
        email,
        creation: Date.now(),
      });
      resolve(user);
    } catch (error) {
      reject(new DatabaseError('could not create new database record'));
    }
  });
};

const updateUser = async(store, issuer, claims) => {
  return new Promise(async(resolve, reject) => {
    if(!claims || !issuer) reject(new InvalidArgument('invalid arguments'))
    try {
      const user = await getUser(store, issuer);
      user[claims.type] = claims.value;
      const updatedUser = await repository.setUser(store, issuer, user);
      resolve(updatedUser);
    } catch (error) {
      console.log(error);
      reject(new DatabaseError("could not update user record"));
    }
  });
};

const getUser = (store, id) => {
  return new Promise(async(resolve, reject) => {
    if(!id) reject(new InvalidArgument('invalid arguments'));
    try {
      const user = await repository.getUser(store, id);
      if(!user) {
        reject(new UserNotFound('could not located user with: ' + id));
      } else {
        resolve(user);
      }
    } catch (error) {
      reject(new DatabaseError("Error getting document:", error));
    }
  });
};

const deleteUser = (store, id) => {
  return new Promise(async(resolve, reject) => {
    if(!id) reject(new InvalidArgument('invalid arguments'));
    try {
      const user = await repository.deleteUser(store, id);
      if(!user) {
        reject(new DatabaseError('could not located user with: ' + id));
      } else {
        resolve(user);
      }
    } catch (error) {
      reject(new DatabaseError("Error getting document:", error));
    }
  });
};

app.get('/api/v1/claims/:uuid/:did', async(req, res) => {
  messageLogger('uuid: ', req.params.uuid);
  messageLogger('clientDid: ', req.params.did);

  const uuid = req.params.uuid;
  const did  = req.params.did;

  if(!uuid) {
    logger.error('uuid not found in request');
    return res.status(400).send('uuid not found in request');
  }

  if(!did) {
    logger.error('did not found in request');
    return res.status(400).send('did not found in request');
  }

  try {
    const user = await getUser(store, uuid);
    await createNewUserRecord(store, {did, phone: user.phone, email: user.email, uuid});

    const signingRequests = ['email', 'phone'].map(key => {
      return signer.signJWT({audience: did, claims: [{type: key, value: user[key]}]});
    });

    const claims = await Promise.all(signingRequests);
    return res.json(claims);

  } catch (error) {
    logger.error(error.stack);
    if (error instanceof DatabaseError) {
      return res.status(500).send({ message: 'database problems', error });
    } else if (error instanceof UserNotFound) {
      return res.status(404).send({ error: 'user not found in registry' });
    } else {
      return res.status(500).send({ message: 'internal server error', error });
    }
  }
});

app.get('/api/v1/claims', async(req, res) => {
  const jwtRequestFromUser  = req.query.jwt;
  try {
    const decodedJwt    = await verify(jwtRequestFromUser);
    const issuer        = decodedJwt.payload.iss;

    const user          = await getUser(store, issuer);
    const signingRequests = ['email', 'phone'].map(key => signer.signJWT({audience: issuer, claims: [{type: key, value: user[key]}]}));

    const claims = await Promise.all(signingRequests);
    return res.json(claims);

  } catch (error) {
    logger.error(error.stack);
    if (error instanceof DatabaseError) {
      return res.status(500).send({ message: 'database problems', error: error.stack });
    } else if (error instanceof UserNotFound) {
      return res.status(404).send({ error: 'user not found in registry' });
    } else if(error instanceof JwtValidation) {
      return res.status(400).send({ error: 'cant validate request (jwt not valid)' });
    } else {
      return res.status(500).send({ error: 'internal server error' });
    }
  }
});
app.put('/api/v1/claims', async(req, res) => {
  const jwtRequestFromUser  = req.body.jwt;
  try {
    const decodedJwt        = await verify(jwtRequestFromUser);
    const {iss, claims}     = decodedJwt.payload;

    messageLogger('issuer', iss);
    messageLogger('claims', claims);

    const user = await updateUser(store, iss, claims);
    return res.json(user);
  } catch (error) {
    if (error instanceof DatabaseError) {
      return res.status(404).send({ error: 'user not present in database' });
    } else if(error instanceof JwtValidation) {
      return res.status(400).send({ error: 'cant validate request (jwt not valid)' });
    } else {
      return res.status(500).send({ error: 'internal server error' });
    }
  }
});


app.delete('/api/v1/claims', async(req, res) => {
  const jwtRequestFromUser  = req.body.jwt;
  try {
    const decodedJwt        = await verify(jwtRequestFromUser);
    const {iss}             = decodedJwt.payload;

    messageLogger('issuer', iss);

    const user = await deleteUser(store, iss);
    return res.status(200).send();
  } catch (error) {
    if (error instanceof DatabaseError) {
      return res.status(404).send({ error: 'user not present in database' });
    } else if(error instanceof JwtValidation) {
      return res.status(400).send({ error: 'cant validate request (jwt not valid)' });
    } else {
      return res.status(500).send({ error: 'internal server error' });
    }
  }
});

app.server.listen(process.env.PORT || config.port, () => {
  console.log(`started express server on port ${app.server.address().port}`);
});
