FROM node:10

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

#install the pm2 runtime
RUN npm install pm2 -g

RUN npm install
# If you are building your code for production
# RUN npm install --only=production


ARG environment=production
ENV NODE_ENV=$environment

ARG web3provider=http://46.101.230.136:8501
ENV DEFAULT_WEB3_PROVIDER=web3provider

# Bundle app source
COPY . .

EXPOSE 8083
CMD ["pm2-runtime", "process.json", "--env", "production"]
