const db                = require('./leveldb');
const {DatabaseError}   = require('../shared/errors');

const getUser = async (store, ssn) => {
  try {
    const buffer = await db.getUser(store, ssn);
    return JSON.parse(buffer.toString());
  } catch (error) {
    return null;
  }
};

const setUser = async (store, ssn, obj) => {
  try {
    await db.setUser(store, ssn, obj);
    const user = await getUser(store, ssn);
    return user;
  } catch (error) {
    throw new DatabaseError(error);
  }
};

const deleteUser = async(store, ssn) => {
  try {
    await db.deleteUser(store, ssn);
    return {};
  } catch (error) {
    throw new DatabaseError(error);
  }
};


module.exports = {
  getUser,
  setUser,
  deleteUser
};
