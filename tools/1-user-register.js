require('dotenv').config({path: '../.env'});

const request = require('request');
const EthrDID = require('ethr-did');
const didJWT  = require('did-jwt');
const Wallet  = require('../wallet');
const web3    = require('./web3');

const KONSENT_REGISTRY_USER_URL   = process.env.KONSENT_REGISTRY_USER_URL;
const MOCK_USER_PUB_KEY = '0x903d305e5f9783eeb977d5b1a4c4d690dd1b2000da50773c16ca81e51c6d881a1dc2eab40a2a5863ede4bd146c6355117693745d35fa6ecf4a3a9a6c10a1e1ad';
const PASSWORD = '123';
const MNEMONIC = 'glare modify fine lazy practice flee equip foam message insect camp approve';

const createRegistryEntry = entry => {
  console.log(KONSENT_REGISTRY_USER_URL);
  console.log('creating fake user registry entry', entry);
  return new Promise((resolve, reject) => {
    request({
      url: KONSENT_REGISTRY_USER_URL,
      method: 'POST',
      json: {
        ...entry
      }
    }, (error, response, body) => {
      if(error) {
        reject(error);
      } else {
        resolve(body);
      }
    });
  });
};

(async () => {
  const userWallet    = await Wallet.create(MNEMONIC, PASSWORD);

  const user = new EthrDID({
    address: userWallet.address,
    privateKey: userWallet.privateKey.substring(2),
    provider: web3.currentProvider
  });

  const entry = {
    address: user.address,
    did: user.did,
    name: "John Doe",
    phone: "97102265",
    publicKey: MOCK_USER_PUB_KEY
  };

  try {
    await createRegistryEntry(entry);
  } catch (error) {
    console.error(error);
  }
}
)();

