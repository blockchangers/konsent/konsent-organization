require('dotenv').config({path: '../.env'});

const request = require('request');

const getOrganizationListing = () => {
  return new Promise((resolve, reject) => {
    request({
      url: process.env.KONSENT_REGISTRY_ORG_URL,
      method: 'GET',
    }, (error, response, body) => {
      if(error) {
        reject(error);
      } else {
        resolve(JSON.parse(body));
      }
    });
  });
};

(async () => {
  try {
    const organizations = await getOrganizationListing();
    console.log(organizations);
  } catch (error) {
    console.error(error);
  }
}
)();

