require('dotenv').config({path: '../.env'});

const request = require('request');
const EthrDID = require('ethr-did');
const didJWT  = require('did-jwt');
const Wallet  = require('../wallet');
const web3    = require('./web3');
require('ethr-did-resolver')();

const PASSWORD = '123';
const MNEMONIC = 'glare modify fine lazy practice flee equip foam message insect camp approve';

const deleteUser = (jwt) => {
  return new Promise((resolve, reject) => {
    request({
      url: process.env.KONSENT_ONBOARDING_URL + '/api/v1/claims',
      method: 'DELETE',
      json: {
        jwt
      }
    }, (error, response, body) => {
      if(error) {
        reject(error);
      } else {
        resolve(body);
      }
    });
  });
};

(async () => {
  try {
    const userWallet = await Wallet.create(MNEMONIC, PASSWORD);

    const user = new EthrDID({
      address: userWallet.address,
      privateKey: userWallet.privateKey.substring(2),
      provider: web3.currentProvider
    });

    const organizationDID = 'did:ethr:0xF2d7e3D1d51Ad442357221377cE462ba1B350a58';
    const signedUserJwt   = await user.signJWT({audience: organizationDID});

    const claims = await deleteUser(signedUserJwt);
    console.log(claims)

  } catch (error) {
    console.error(error);
  }
}
)();
