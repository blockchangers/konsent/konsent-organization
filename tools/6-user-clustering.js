require('dotenv').config({path: '../.env'});

const request           = require('request');
const R                 = require('ramda');
const EthrDID           = require('ethr-did');
const didJWT            = require('did-jwt');
const Wallet            = require('../wallet');
const web3              = require('./web3');
const {messageLogger}   = require('../logger');

require('ethr-did-resolver')();

const {extract, groupByType, countBy, totalNumberOfOccurences, withPercentage, rank}  = require('../clustering');

const PASSWORD = '123';
const MNEMONIC = 'glare modify fine lazy practice flee equip foam message insect camp approve';

const getOrganizationListing = () => {
  return new Promise((resolve, reject) => {
    request({
      url: process.env.KONSENT_REGISTRY_ORG_URL,
      method: 'GET',
    }, (error, response, body) => {
      if(error) {
        reject(error);
      } else {
        resolve(JSON.parse(body));
      }
    });
  });
};


const getClaims = (callbackUrl, jwt) => {
  return new Promise((resolve, reject) => {
    request({
      url: callbackUrl + '/?jwt=' + jwt ,
      method: 'GET',
    }, (error, response, body) => {
      if(error) {
        reject(error);
      } else {
        console.log(body);
        resolve(JSON.parse(body));
      }
    });
  });
};

(async () => {
  try {
    const userWallet = await Wallet.create(MNEMONIC, PASSWORD);

    const user = new EthrDID({
      address: userWallet.address,
      privateKey: userWallet.privateKey.substring(2),
      provider: web3.currentProvider
    });

    const organizations = await getOrganizationListing();

    const keys = R.keys(organizations);
    const targets = keys.map(did => organizations[did]);

    const jwts = targets.map(organization => user.signJWT({audience: organization.did}));
    const signedJwts = await Promise.all(jwts);

    const requests = targets.map((organization,index) => getClaims(organization.callbackUrl, signedJwts[index]))

    const results = await Promise.all(requests);
    const allJwts = R.flatten(results);

    const verifiedUserJwts = await Promise.all(allJwts.map(jwt => didJWT.verifyJWT(jwt)));

    // simulate different prop
    verifiedUserJwts[0].payload.claims[0].value = 'blah@blah.com';

    const claims = verifiedUserJwts.map(jwt => extract(jwt));

    const byType = groupByType(R.flatten(claims));

    messageLogger("Claims",byType);
    //console.dir(byType,{depth:null})

    //email
    const emailCount = countBy(claim => claim.value.toLowerCase(), byType.email)
    console.dir(emailCount,{depth:null})
    const emailTotal = totalNumberOfOccurences(emailCount);
    const emailScore = withPercentage(emailTotal, emailCount);
    const emailRanking = rank(Object.entries(emailScore).map(([key, value]) => ({key,value})));
    messageLogger("Email", emailRanking);

    //phone
    const phoneCount = countBy(claim => claim.value.toLowerCase(), byType.phone)
    console.dir(phoneCount,{depth:null})
    const phoneTotal = totalNumberOfOccurences(phoneCount);
    const phoneScore = withPercentage(phoneTotal, phoneCount);
    const phoneRanking = rank(Object.entries(phoneScore).map(([key, value]) => ({key,value})));
    messageLogger("Telephone", phoneRanking);

  } catch (error) {
    console.error(error);
  }
}
)();
