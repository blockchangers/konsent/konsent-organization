require('dotenv').config({path: '../.env'});

const request = require('request');
const EthrDID = require('ethr-did');
const didJWT  = require('did-jwt');
const Wallet  = require('../wallet');
const web3    = require('./web3');

require('ethr-did-resolver')();

const PASSWORD = '123';
const MNEMONIC = 'glare modify fine lazy practice flee equip foam message insect camp approve';

const getInitialClaims = (did, uuid) => {
  return new Promise((resolve, reject) => {
    request({
      url: `${process.env.KONSENT_ONBOARDING_URL}/api/v1/claims/${uuid}/${did}`,
      method: 'GET',
    }, (error, response, body) => {
      if(error) {
        reject(error);
      } else {
        resolve(JSON.parse(body));
      }
    });
  });
};

(async () => {
  try {
    const userWallet = await Wallet.create(MNEMONIC, PASSWORD);

    const user = new EthrDID({
      address: userWallet.address,
      privateKey: userWallet.privateKey.substring(2),
      provider: web3.currentProvider
    });

    const uuid = '03c73950-03cd-4087-b5bf-8919e38f714d';

    const claims = await getInitialClaims(user.did, uuid);

    const verifyOps = claims.map(claim => didJWT.verifyJWT(claim));

    Promise.all(verifyOps)
    .then(verifiedJwt => {
      console.dir(verifiedJwt,{depth:null})
    })
    .catch(console.error.bind(this));

  } catch (error) {
    console.error(error);
  }
}
)();
