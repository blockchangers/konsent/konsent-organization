require('dotenv').config({path: '../.env'});

const request                   = require('request');
const EthrDID                   = require('ethr-did');
const didJWT                    = require('did-jwt');
const Wallet                    = require('../wallet');
const web3                      = require('./web3');
const {logger, messageLogger}   = require('../logger');
require('ethr-did-resolver')();

const PASSWORD = '123';
const MNEMONIC = 'glare modify fine lazy practice flee equip foam message insect camp approve';

const setClaims = (jwt) => {
  return new Promise((resolve, reject) => {
    request({
      url: process.env.KONSENT_ONBOARDING_URL + '/api/v1/claims',
      method: 'PUT',
      json: { jwt }
    }, (error, response, body) => {
      if(error) {
        reject(error);
      } else {
        resolve(body);
      }
    });
  });
};

(async () => {
  try {
    const userWallet = await Wallet.create(MNEMONIC, PASSWORD);

    const user = new EthrDID({
      address: userWallet.address,
      privateKey: userWallet.privateKey.substring(2),
      provider: web3.currentProvider
    });

    const audience = 'did:ethr:0xf52e4D9e8e94c85661aF15Da5AcA7520B2259D21';
    const signedUserJwt   = await user.signJWT({audience, claims: { type: 'phone', value: '666'}});
    messageLogger("Decoded request", didJWT.decodeJWT(signedUserJwt));

    const response = await setClaims(signedUserJwt);
    messageLogger("Updated user record", response);
  } catch (error) {
    console.error(error);
  }
}
)();
