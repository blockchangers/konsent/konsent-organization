require('dotenv').config();

const Web3 = require('web3');

const DEFAULT_WEB3_PROVIDER = process.env.DEFAULT_WEB3_PROVIDER;

const web3 = new Web3(new Web3.providers.HttpProvider(DEFAULT_WEB3_PROVIDER));

module.exports = web3;

